package model

import "github.com/jinzhu/gorm"

// Account model
type Account struct {
	gorm.Model
	OwnerID uint   `gorm:"not null; unique_index"`
	Name    string `gorm:"not null"`
	Slug    string `gorm:"not null; unique_index"`
	Owner   *User  `gorm:"foreignkey:OwnerID"`
}
