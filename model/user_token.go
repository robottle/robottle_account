package model

import "github.com/jinzhu/gorm"

// UserToken model.
type UserToken struct {
	gorm.Model
	UserID uint   `gorm:"not null"`
	Token  string `gorm:"not null; unique_index"`
	User   *User
}
