package model

import "github.com/jinzhu/gorm"

// User model.
type User struct {
	gorm.Model
	AccountID    uint         `gorm:"not null; index"`
	Name         string       `gorm:"not null"`
	Email        string       `gorm:"not null; unique_index"`
	PasswordHash string       `gorm:"not null" json:"-"`
	Account      *Account     `gorm:"foreignkey:AccountID"`
	Tokens       []*UserToken `json:"-"`
}
