package router

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/robottle/robottle_account/middleware"
	"gitlab.com/robottle/robottle_common/config"

	"gitlab.com/robottle/robottle_account/option"
)

// NewRouter returns the API router.
func NewRouter(options *option.Options) *gin.Engine {
	router := gin.Default()
	if config.IsProduction() {
		gin.SetMode(gin.ReleaseMode)
	}
	router.RedirectTrailingSlash = true

	router.Use(cors.New(cors.Config{
		AllowAllOrigins: false,
		AllowOrigins:    options.AllowedHosts,
		AllowHeaders:    options.AllowedHeaders,
		AllowMethods:    options.AllowedMethods,
		ExposeHeaders: []string{
			"Content-Type",
			"Content-Length",
			"Access-Control-Allow-Headers",
			"Access-Control-Allow-Methods",
		},
		AllowOriginFunc: func(origin string) bool {
			return true
		},
	}))

	requireToken := middleware.RequireToken(options.AuthService)

	accountHandler := NewAccountHandler(options)
	userHandler := NewUserHandler(options)
	authHandler := NewAuthHandler(options)

	account := router.Group("/account")

	accounts := account.Group("/accounts")
	{
		accounts.GET("/", accountHandler.Index)
		accounts.POST("/", accountHandler.Create)
	}

	users := account.Group("/users")
	{
		users.POST("/", requireToken, userHandler.Create)
		users.GET("/", requireToken, userHandler.Index)
		users.PUT("/:id", requireToken, userHandler.Update)
	}

	auth := account.Group("/auth")
	{
		auth.POST("/", authHandler.Create)
		auth.GET("/", requireToken, authHandler.Show)
		auth.DELETE("/", requireToken, authHandler.Delete)
	}

	return router
}
