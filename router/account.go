package router

import (
	"net/http"

	"gitlab.com/robottle/robottle_account/validate"

	"github.com/gin-gonic/gin"

	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
)

type AccountHandler struct {
	accountService proto.AccountService
}

func NewAccountHandler(options *option.Options) *AccountHandler {
	return &AccountHandler{
		accountService: options.AccountService,
	}
}

func (h *AccountHandler) Create(ctx *gin.Context) {
	req := &proto.AccountRequest{}
	if err := ctx.ShouldBindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	requestValidation := &validate.AccountRequestValidation{
		Name: req.Name,
		Owner: &validate.UserRequestValidation{
			Name:                 req.Owner.Name,
			Email:                req.Owner.Email,
			Password:             req.Owner.Password,
			PasswordConfirmation: req.Owner.PasswordConfirmation,
		},
	}
	if err := requestValidation.Validate(); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	res, err := h.accountService.CreateAccount(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusCreated, res)
}

func (h *AccountHandler) Index(ctx *gin.Context) {
	req := &proto.ListAccountsRequest{}
	res, err := h.accountService.ListAccounts(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status": http.StatusInternalServerError,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusOK, res)
}
