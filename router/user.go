package router

import (
	"net/http"
	"strconv"

	"gitlab.com/robottle/robottle_account/validate"

	"github.com/gin-gonic/gin"
	"gitlab.com/robottle/robottle_account/helper"

	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
)

type UserHandler struct {
	userService proto.UserService
}

func NewUserHandler(options *option.Options) *UserHandler {
	return &UserHandler{
		userService: options.UserService,
	}
}

func (h *UserHandler) Create(ctx *gin.Context) {
	currentUser := helper.GetUser(ctx)
	req := &proto.UserRequest{}
	if err := ctx.ShouldBindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	requestValidation := &validate.UserRequestValidation{
		AccountID:            currentUser.AccountId,
		Name:                 req.Name,
		Email:                req.Email,
		Password:             req.Password,
		PasswordConfirmation: req.PasswordConfirmation,
	}
	if err := requestValidation.ValidateCreate(); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	req.AccountId = currentUser.AccountId
	res, err := h.userService.CreateUser(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusCreated, res)
}

func (h *UserHandler) Update(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 32)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  "invalid user id",
		})
		return
	}
	req := &proto.UserRequest{}
	if err := ctx.ShouldBindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	currentUser := helper.GetUser(ctx)
	requestValidation := &validate.UserRequestValidation{
		ID:                   id,
		AccountID:            currentUser.AccountId,
		Name:                 req.Name,
		Password:             req.Password,
		PasswordConfirmation: req.PasswordConfirmation,
	}
	req.AccountId = currentUser.AccountId
	if err := requestValidation.ValidateUpdate(); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	res, err := h.userService.UpdateUser(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusOK, res)
}

func (h *UserHandler) Index(ctx *gin.Context) {
	currentUser := helper.GetUser(ctx)
	req := &proto.ListUsersRequest{
		AccountId: currentUser.AccountId,
	}
	res, err := h.userService.ListUsers(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status": http.StatusInternalServerError,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusOK, res)
}
