package router

import (
	"net/http"

	"gitlab.com/robottle/robottle_account/helper"

	"gitlab.com/robottle/robottle_account/validate"

	"github.com/gin-gonic/gin"
	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
)

type AuthHandler struct {
	authService proto.AuthService
}

func NewAuthHandler(options *option.Options) *AuthHandler {
	return &AuthHandler{
		authService: options.AuthService,
	}
}

func (h *AuthHandler) Create(ctx *gin.Context) {
	req := &proto.CreateTokenRequest{}
	if err := ctx.ShouldBindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	requestValidation := &validate.AuthRequestValidation{
		Email:    req.Email,
		Password: req.Password,
	}
	if err := requestValidation.Validate(); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	res, err := h.authService.CreateToken(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusCreated, res)
}

func (h *AuthHandler) Show(ctx *gin.Context) {
	token := helper.GetToken(ctx)
	req := &proto.VerifyTokenRequest{
		Token: token,
	}
	res, err := h.authService.VerifyToken(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusOK, res)
}

func (h *AuthHandler) Delete(ctx *gin.Context) {
	token := helper.GetToken(ctx)
	req := &proto.RevokeTokenRequest{
		Token: token,
	}
	_, err := h.authService.RevokeToken(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status": http.StatusInternalServerError,
			"error":  err,
		})
	}
	ctx.Status(http.StatusNoContent)
}
