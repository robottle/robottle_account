package handler

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitlab.com/robottle/robottle_common/auth"

	"github.com/jinzhu/gorm"
	"gitlab.com/robottle/robottle_common/hashing"

	"gitlab.com/robottle/robottle_account/model"
	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/repository"
	"gitlab.com/robottle/robottle_account/validate"
)

// UserHandler implements `proto.UserHandler`.
type UserHandler struct {
	userRepository  *repository.UserRepository
	hashingStrategy hashing.Strategy
	authStrategy    auth.Strategy
}

// NewUserHandler returns an implementation of `proto.UserHandler`.
func NewUserHandler(options *option.Options) proto.UserHandler {
	return &UserHandler{
		userRepository:  options.UserRepository,
		hashingStrategy: options.HashingStrategy,
		authStrategy:    options.AuthStrategy,
	}
}

// CreateUser creates a new user.
func (h *UserHandler) CreateUser(ctx context.Context, req *proto.UserRequest, res *proto.UserResponse) error {
	requestValidation := &validate.UserRequestValidation{
		AccountID:            req.AccountId,
		Name:                 req.Name,
		Email:                req.Email,
		Password:             req.Password,
		PasswordConfirmation: req.PasswordConfirmation,
	}
	if err := requestValidation.ValidateCreate(); err != nil {
		return err
	}
	passwordHash, err := h.hashingStrategy.GenerateFromPassword(req.Password)
	if err != nil {
		return fmt.Errorf("error generating password hash: %v", err)
	}
	user := &model.User{
		AccountID:    uint(req.AccountId),
		Name:         req.Name,
		Email:        req.Email,
		PasswordHash: passwordHash,
	}
	if err := h.userRepository.CreateUser(user); err != nil {
		return fmt.Errorf("error creating user: %v", err)
	}
	res.Id = int64(user.ID)
	res.AccountId = int64(user.AccountID)
	res.Name = user.Name
	res.Email = user.Email
	res.CreatedAt = user.CreatedAt.Format(time.RFC3339)
	res.UpdatedAt = user.UpdatedAt.Format(time.RFC3339)
	return nil
}

// UpdateUser updates an existing user.
func (h *UserHandler) UpdateUser(ctx context.Context, req *proto.UserRequest, res *proto.UserResponse) error {
	user, err := h.userRepository.FindUserByID(req.Id)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return fmt.Errorf("user with id %v not found", req.Id)
		}
		return err
	}
	updates := make(map[string]interface{})
	if len(strings.TrimSpace(req.Name)) > 0 {
		updates["name"] = req.Name
	}
	if len(strings.TrimSpace(req.Password)) > 0 {
		passwordHash, err := h.hashingStrategy.GenerateFromPassword(req.Password)
		if err != nil {
			return err
		}
		updates["password_hash"] = passwordHash
	}
	if len(updates) > 0 {
		if err := h.userRepository.UpdateUser(user, updates); err != nil {
			return err
		}
	}
	res.Id = int64(user.ID)
	res.AccountId = int64(user.AccountID)
	res.Name = user.Name
	res.Email = user.Email
	res.CreatedAt = user.CreatedAt.Format(time.RFC3339)
	res.UpdatedAt = user.UpdatedAt.Format(time.RFC3339)
	return nil
}

func (h *UserHandler) ListUsers(ctx context.Context, req *proto.ListUsersRequest, res *proto.ListUsersResponse) error {
	users, err := h.userRepository.ListUsersByAccountID(req.AccountId)
	if err != nil {
		return err
	}
	res.AccountId = req.AccountId
	res.Users = make([]*proto.UserResponse, len(users))
	for i, user := range users {
		res.Users[i] = &proto.UserResponse{
			Id:        int64(user.ID),
			AccountId: int64(user.AccountID),
			Name:      user.Name,
			Email:     user.Email,
			CreatedAt: user.CreatedAt.Format(time.RFC3339),
			UpdatedAt: user.UpdatedAt.Format(time.RFC3339),
		}
	}
	return nil
}
