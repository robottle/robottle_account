package handler

import (
	"context"
	"fmt"
	"testing"

	"github.com/brianvoe/gofakeit"

	"github.com/stretchr/testify/suite"

	"gitlab.com/robottle/robottle_common/connection"
	"gitlab.com/robottle/robottle_common/hashing"
	commonTesting "gitlab.com/robottle/robottle_common/testing"

	"gitlab.com/robottle/robottle_account/model"
	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/repository"
)

type accountHandlerSuite struct {
	commonTesting.Suite
	serviceSuite

	handler proto.AccountHandler
}

func (suite *accountHandlerSuite) SetupSuite() {
	r := suite.Require()

	suite.Init()

	suite.DeleteDatabaseFile("account")

	db, err := connection.GetDatabaseConnection("account")
	r.Nilf(err, "error getting database connection: %v", err)

	suite.MigrateModels(
		"account",
		&model.Account{},
		&model.User{},
	)

	hashingStrategy, err := hashing.NewHashingStrategy(suite.Config.Hashing)
	r.Nilf(err, "error getting hashing strategy: %v", err)

	accountRepository := repository.NewAccountRepository(db)
	userRepository := repository.NewUserRepository(db)

	options := option.NewOptions(
		option.WithHashingStrategy(hashingStrategy),
		option.WithAccountRepository(accountRepository),
		option.WithUserRepository(userRepository),
	)

	suite.handler = NewAccountHandler(options)
}

func (suite *accountHandlerSuite) SetupTest() {
	suite.TruncateTables(
		"account",
		"accounts",
		"users",
	)
}

func (suite *accountHandlerSuite) TearDownSuite() {
	suite.DeleteDatabaseFile("account")
}

func (suite *accountHandlerSuite) TestCreateAccount() {
	r := suite.Require()

	ctx := context.TODO()

	person := gofakeit.Person()
	password := gofakeit.Password(true, true, true, true, false, 8)
	req := &proto.AccountRequest{
		Name: gofakeit.Company(),
		Owner: &proto.UserRequest{
			Name:                 fmt.Sprintf("%s %s", person.FirstName, person.LastName),
			Email:                person.Contact.Email,
			Password:             password,
			PasswordConfirmation: password,
		},
	}
	res := &proto.AccountResponse{}
	err := suite.handler.CreateAccount(ctx, req, res)
	r.Nilf(err, "error creating account: %v", err)

	r.NotZero(res.Id)
	r.NotNil(res.Owner)
	r.NotZero(res.Owner.Id)
	r.NotZero(res.Owner.AccountId)
	r.Equal(res.Id, res.Owner.AccountId)
}

func TestAccountHandlerSuite(t *testing.T) {
	suite.Run(t, &accountHandlerSuite{})
}
