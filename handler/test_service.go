package handler

import (
	"fmt"
	"time"

	"github.com/brianvoe/gofakeit"

	"gitlab.com/robottle/robottle_common/auth"
	"gitlab.com/robottle/robottle_common/hashing"
	commonTesting "gitlab.com/robottle/robottle_common/testing"

	"gitlab.com/robottle/robottle_account/model"
	"gitlab.com/robottle/robottle_account/repository"
)

type serviceSuite struct {
	commonTesting.Suite

	authStrategy        auth.Strategy
	hashingStrategy     hashing.Strategy
	accountRepository   *repository.AccountRepository
	userRepository      *repository.UserRepository
	userTokenRepository *repository.UserTokenRepository
}

func init() {
	gofakeit.Seed(time.Now().UnixNano())
}

func (suite *serviceSuite) createToken(email string, password string) string {
	r := suite.Require()

	user, err := suite.userRepository.FindUserByEmail(email)
	r.Nilf(err, "error getting user: %v", err)

	data := map[string]interface{}{
		"user_id":    user.ID,
		"account_id": user.AccountID,
		"email":      user.Email,
	}
	token, err := suite.authStrategy.GenerateToken(data)
	r.Nilf(err, "error generating token: %v", err)

	userToken := &model.UserToken{
		UserID: user.ID,
		Token:  token,
	}
	err = suite.userTokenRepository.CreateUserToken(userToken)
	r.Nilf(err, "error creating user token: %v", err)

	return token
}

func (suite *serviceSuite) createUser(account *model.Account, password string) *model.User {
	r := suite.Require()

	person := gofakeit.Person()
	name := fmt.Sprintf("%s %s", person.FirstName, person.LastName)
	passwordHash, err := suite.hashingStrategy.GenerateFromPassword(password)
	r.Nilf(err, "error generating password hash: %v", err)

	user := &model.User{
		AccountID:    account.ID,
		Name:         name,
		Email:        person.Contact.Email,
		PasswordHash: passwordHash,
	}
	err = suite.userRepository.CreateUser(user)
	r.Nilf(err, "error creating user: %v", err)

	return user
}

func (suite *serviceSuite) createAccount(password string) *model.Account {
	r := suite.Require()

	company := gofakeit.Company()
	owner := gofakeit.Person()
	name := fmt.Sprintf("%s %s", owner.FirstName, owner.LastName)

	passwordHash, err := suite.hashingStrategy.GenerateFromPassword(password)
	r.Nilf(err, "error hashing password: %v", err)

	account := &model.Account{
		Name: company,
		Owner: &model.User{
			Name:         name,
			Email:        owner.Contact.Email,
			PasswordHash: passwordHash,
		},
	}
	err = suite.accountRepository.CreateAccount(account)
	r.Nilf(err, "error creating account")

	return account
}
