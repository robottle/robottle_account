package handler

import (
	"context"
	"fmt"
	"reflect"
	"time"

	"github.com/jinzhu/gorm"

	"github.com/micro/go-micro"

	"gitlab.com/robottle/robottle_common/hashing"

	"gitlab.com/robottle/robottle_account/model"

	"gitlab.com/robottle/robottle_account/validate"

	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/repository"
)

// AccountHandler implements `proto.AccountHandler`
type AccountHandler struct {
	accountRepository       *repository.AccountRepository
	userRepository          *repository.UserRepository
	hashingStrategy         hashing.Strategy
	accountCreatedPublisher micro.Publisher
}

// NewAccountHandler returns an implementation of `proto.AccountHandler`.
func NewAccountHandler(options *option.Options) proto.AccountHandler {
	return &AccountHandler{
		accountRepository:       options.AccountRepository,
		userRepository:          options.UserRepository,
		hashingStrategy:         options.HashingStrategy,
		accountCreatedPublisher: options.AccountCreatedPublisher,
	}
}

// CreateAccount creates a new account.
func (h *AccountHandler) CreateAccount(ctx context.Context, req *proto.AccountRequest, res *proto.AccountResponse) error {
	validation := validate.AccountRequestValidation{
		Name: req.Name,
		Owner: &validate.UserRequestValidation{
			Name:                 req.Owner.Name,
			Email:                req.Owner.Email,
			Password:             req.Owner.Password,
			PasswordConfirmation: req.Owner.PasswordConfirmation,
		},
	}
	if err := validation.Validate(); err != nil {
		return err
	}
	exists, err := h.userRepository.FindUserByEmail(req.Owner.Email)
	if err != nil {
		if !gorm.IsRecordNotFoundError(err) {
			return err
		}
	}
	if !reflect.DeepEqual(exists, &model.User{}) {
		return fmt.Errorf("email %s already taken", req.Owner.Email)
	}
	passwordHash, err := h.hashingStrategy.GenerateFromPassword(req.Owner.Password)
	if err != nil {
		return err
	}
	slug, err := h.accountRepository.GenerateSlug(req.Name)
	if err != nil {
		return err
	}
	account := &model.Account{
		Name: req.Name,
		Slug: slug,
		Owner: &model.User{
			Name:         req.Owner.Name,
			Email:        req.Owner.Email,
			PasswordHash: passwordHash,
		},
	}
	if err := h.accountRepository.CreateAccount(account); err != nil {
		return err
	}
	res.Id = int64(account.ID)
	res.Name = account.Name
	res.Slug = account.Slug
	res.Owner = &proto.UserResponse{
		Id:        int64(account.Owner.ID),
		AccountId: int64(account.Owner.AccountID),
		Name:      account.Owner.Name,
		Email:     account.Owner.Email,
		CreatedAt: account.Owner.CreatedAt.Format(time.RFC3339),
		UpdatedAt: account.Owner.UpdatedAt.Format(time.RFC3339),
	}
	res.CreatedAt = account.CreatedAt.Format(time.RFC3339)
	res.UpdatedAt = account.UpdatedAt.Format(time.RFC3339)
	if err := h.accountCreatedPublisher.Publish(ctx, res); err != nil {
		return err
	}
	return nil
}

// ListAccounts returns a list of active accounts.
func (h *AccountHandler) ListAccounts(ctx context.Context, req *proto.ListAccountsRequest, res *proto.ListAccountsResponse) error {
	accounts, err := h.accountRepository.ListAccounts()
	if err != nil {
		return err
	}
	res.Accounts = make([]*proto.AccountResponse, len(accounts))
	for i, account := range accounts {
		res.Accounts[i] = &proto.AccountResponse{
			Id:      int64(account.ID),
			OwnerId: int64(account.OwnerID),
			Owner: &proto.UserResponse{
				Id:        int64(account.Owner.ID),
				AccountId: int64(account.Owner.AccountID),
				Name:      account.Owner.Name,
				Email:     account.Owner.Email,
				CreatedAt: account.Owner.CreatedAt.Format(time.RFC3339),
				UpdatedAt: account.Owner.UpdatedAt.Format(time.RFC3339),
			},
			Name:      account.Name,
			Slug:      account.Slug,
			CreatedAt: account.CreatedAt.Format(time.RFC3339),
			UpdatedAt: account.UpdatedAt.Format(time.RFC3339),
		}
	}
	return nil
}

func (h *AccountHandler) FindAccountByID(ctx context.Context, req *proto.FindAccountRequest, res *proto.AccountResponse) error {
	account, err := h.accountRepository.FindAccountByID(req.Id)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return fmt.Errorf("account with id %s not found", req.Id)
		}
		return err
	}
	res.Id = int64(account.ID)
	res.Name = account.Name
	res.Slug = account.Slug
	res.CreatedAt = account.CreatedAt.Format(time.RFC3339)
	res.UpdatedAt = account.UpdatedAt.Format(time.RFC3339)
	return nil
}
