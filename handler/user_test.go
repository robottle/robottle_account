package handler

import (
	"context"
	"fmt"
	"testing"

	"gitlab.com/robottle/robottle_common/auth"

	"github.com/brianvoe/gofakeit"

	"github.com/stretchr/testify/suite"

	"gitlab.com/robottle/robottle_account/model"
	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/repository"
	"gitlab.com/robottle/robottle_common/connection"
	"gitlab.com/robottle/robottle_common/hashing"
)

type userHandlerSuite struct {
	serviceSuite

	handler proto.UserHandler
}

func (suite *userHandlerSuite) SetupSuite() {
	r := suite.Require()

	suite.Init()

	suite.DeleteDatabaseFile("account")

	db, err := connection.GetDatabaseConnection("account")
	r.Nilf(err, "error getting database connection: %v", err)

	suite.MigrateModels(
		"account",
		&model.Account{},
		&model.User{},
		&model.UserToken{},
	)

	hashingStrategy, err := hashing.NewHashingStrategy(suite.Config.Hashing)
	r.Nilf(err, "error getting hashing strategy: %v", err)

	authStrategy, err := auth.NewAuthStrategy(suite.Config.Auth)
	r.Nilf(err, "error getting authentication strategy: %v", err)

	accountRepository := repository.NewAccountRepository(db)
	userRepository := repository.NewUserRepository(db)
	userTokenRepository := repository.NewUserTokenRepository(db)

	options := option.NewOptions(
		option.WithHashingStrategy(hashingStrategy),
		option.WithAuthStrategy(authStrategy),
		option.WithAccountRepository(accountRepository),
		option.WithUserRepository(userRepository),
		option.WithUserTokenRepository(userTokenRepository),
	)

	suite.handler = NewUserHandler(options)
	suite.authStrategy = authStrategy
	suite.accountRepository = accountRepository
	suite.userRepository = userRepository
	suite.userTokenRepository = userTokenRepository
	suite.hashingStrategy = hashingStrategy
}

func (suite *userHandlerSuite) SetupTest() {
	suite.TruncateTables(
		"account",
		"accounts",
		"users",
		"user_tokens",
	)
}

func (suite *userHandlerSuite) TearDownSuite() {
	suite.DeleteDatabaseFile("account")
}

func (suite *userHandlerSuite) TestCreateUser() {
	r := suite.Require()

	owner := gofakeit.Person()
	password := gofakeit.Password(true, true, true, true, false, 8)

	passwordHash, err := suite.hashingStrategy.GenerateFromPassword(password)
	r.Nilf(err, "error hashing password: %v", err)

	account := &model.Account{
		Name: gofakeit.Company(),
		Owner: &model.User{
			Name:         fmt.Sprintf("%s %s", owner.FirstName, owner.LastName),
			Email:        owner.Contact.Email,
			PasswordHash: passwordHash,
		},
	}
	err = suite.accountRepository.CreateAccount(account)
	r.Nilf(err, "error creating account: %v", err)

	token := suite.createToken(account.Owner.Email, password)

	user := gofakeit.Person()
	name := fmt.Sprintf("%s %s", user.FirstName, user.LastName)
	password = gofakeit.Password(true, true, true, true, false, 8)
	ctx := context.WithValue(context.TODO(), "token", token)
	req := &proto.UserRequest{
		AccountId:            int64(account.ID),
		Name:                 name,
		Email:                user.Contact.Email,
		Password:             password,
		PasswordConfirmation: password,
	}
	res := &proto.UserResponse{}
	err = suite.handler.CreateUser(ctx, req, res)
	r.Nilf(err, "error creating user: %v", err)
}

func (suite *userHandlerSuite) TestUpdateUser() {
	r := suite.Require()

	password := gofakeit.Password(true, true, true, true, false, 8)

	account := suite.createAccount(password)

	password = gofakeit.Password(true, true, true, true, false, 8)
	user := suite.createUser(account, password)
	token := suite.createToken(user.Email, password)

	name := gofakeit.Name()
	ctx := context.WithValue(context.TODO(), "token", token)
	req := &proto.UserRequest{
		Id:   int64(user.ID),
		Name: name,
	}
	res := &proto.UserResponse{}
	err := suite.handler.UpdateUser(ctx, req, res)
	r.Nilf(err, "error updating user: %v", err)

	r.Equal(res.Name, name)
	r.EqualValues(user.ID, res.Id)
	r.Equal(user.Email, res.Email)
	r.EqualValues(user.AccountID, res.AccountId)
}

func TestUserHandlerSuite(t *testing.T) {
	suite.Run(t, &userHandlerSuite{})
}
