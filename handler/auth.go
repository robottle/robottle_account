package handler

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/robottle/robottle_account/model"

	"github.com/jinzhu/gorm"
	"gitlab.com/robottle/robottle_common/auth"
	"gitlab.com/robottle/robottle_common/hashing"

	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/repository"
	"gitlab.com/robottle/robottle_account/validate"
)

// AuthHandler implements `proto.AuthHandler`.
type AuthHandler struct {
	userRepository      *repository.UserRepository
	userTokenRepository *repository.UserTokenRepository
	authStrategy        auth.Strategy
	hashingStrategy     hashing.Strategy
}

// NewAuthHandler returns an implementation of `proto.AuthHandler`.
func NewAuthHandler(options *option.Options) proto.AuthHandler {
	return &AuthHandler{
		userRepository:      options.UserRepository,
		userTokenRepository: options.UserTokenRepository,
		authStrategy:        options.AuthStrategy,
		hashingStrategy:     options.HashingStrategy,
	}
}

// CreateToken creates a new user token.
func (h *AuthHandler) CreateToken(ctx context.Context, req *proto.CreateTokenRequest, res *proto.TokenResponse) error {
	requestValidation := &validate.AuthRequestValidation{
		Email:    req.Email,
		Password: req.Password,
	}
	if err := requestValidation.Validate(); err != nil {
		return err
	}
	user, err := h.userRepository.FindUserByEmail(req.Email)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return fmt.Errorf("user not found")
		}
		return err
	}
	match, err := h.hashingStrategy.ComparePasswordAndHash(req.Password, user.PasswordHash)
	if err != nil {
		return err
	}
	if !match {
		return fmt.Errorf("wrong password")
	}
	data := map[string]interface{}{
		"user_id":    user.ID,
		"account_id": user.AccountID,
		"email":      user.Email,
	}
	token, err := h.authStrategy.GenerateToken(data)
	if err != nil {
		return fmt.Errorf("error generating token: %v", err)
	}
	userToken := &model.UserToken{
		UserID: user.ID,
		Token:  token,
	}
	if err := h.userTokenRepository.CreateUserToken(userToken); err != nil {
		return err
	}
	res.Token = token
	res.User = &proto.UserResponse{
		Id:        int64(user.ID),
		AccountId: int64(user.AccountID),
		Name:      user.Name,
		Email:     user.Email,
		CreatedAt: user.CreatedAt.Format(time.RFC3339),
		UpdatedAt: user.UpdatedAt.Format(time.RFC3339),
	}
	res.Claims = map[string]string{
		"user_id":    string(user.ID),
		"account_id": string(user.AccountID),
		"email":      user.Email,
	}
	return nil
}

// VerifyToken checks if a token is valid.
func (h *AuthHandler) VerifyToken(ctx context.Context, req *proto.VerifyTokenRequest, res *proto.TokenResponse) error {
	userToken, err := h.userTokenRepository.FindUserTokenByToken(req.Token)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return fmt.Errorf("invalid token")
		}
		return err
	}
	data, err := h.authStrategy.VerifyToken(req.Token)
	if err != nil {
		if err := h.userTokenRepository.DeleteUserToken(userToken); err != nil {
			return err
		}
		return err
	}
	userID := uint(data["user_id"].(float64))
	user, err := h.userRepository.FindUserByID(userID)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return fmt.Errorf("user with id %v not found", userID)
		}
		return err
	}
	res.User = &proto.UserResponse{
		Id:        int64(user.ID),
		AccountId: int64(user.AccountID),
		Name:      user.Name,
		Email:     user.Email,
		CreatedAt: user.CreatedAt.Format(time.RFC3339),
		UpdatedAt: user.UpdatedAt.Format(time.RFC3339),
	}
	res.Token = req.Token
	res.Claims = make(map[string]string)
	for key, value := range data {
		res.Claims[key] = fmt.Sprintf("%v", value)
	}
	return nil
}

// RevokeToken invalidates a token.
func (h *AuthHandler) RevokeToken(ctx context.Context, req *proto.RevokeTokenRequest, res *proto.RevokeTokenResponse) error {
	userToken, err := h.userTokenRepository.FindUserTokenByToken(req.Token)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil
		}
		return err
	}
	if err := h.userTokenRepository.DeleteUserToken(userToken); err != nil {
		return err
	}
	return nil
}
