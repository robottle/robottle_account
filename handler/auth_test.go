package handler

import (
	"context"
	"errors"
	"testing"

	"github.com/brianvoe/gofakeit"

	"github.com/stretchr/testify/suite"
	"gitlab.com/robottle/robottle_common/auth"
	"gitlab.com/robottle/robottle_common/connection"
	"gitlab.com/robottle/robottle_common/hashing"

	"gitlab.com/robottle/robottle_account/model"
	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/repository"
)

type authHandlerSuite struct {
	serviceSuite

	handler proto.AuthHandler
}

func (suite *authHandlerSuite) SetupSuite() {
	r := suite.Require()

	suite.Init()

	suite.DeleteDatabaseFile("account")

	db, err := connection.GetDatabaseConnection("account")
	r.Nilf(err, "error getting database connection: %v", err)

	suite.MigrateModels(
		"account",
		&model.Account{},
		&model.User{},
		&model.UserToken{},
	)

	hashingStrategy, err := hashing.NewHashingStrategy(suite.Config.Hashing)
	r.Nilf(err, "error getting hashing strategy: %v", err)

	authStrategy, err := auth.NewAuthStrategy(suite.Config.Auth)
	r.Nilf(err, "error getting authentication strategy: %v", err)

	accountRepository := repository.NewAccountRepository(db)
	userRepository := repository.NewUserRepository(db)
	userTokenRepository := repository.NewUserTokenRepository(db)

	options := option.NewOptions(
		option.WithAuthStrategy(authStrategy),
		option.WithHashingStrategy(hashingStrategy),
		option.WithUserTokenRepository(userTokenRepository),
		option.WithUserRepository(userRepository),
	)

	suite.handler = NewAuthHandler(options)
	suite.authStrategy = authStrategy
	suite.hashingStrategy = hashingStrategy
	suite.accountRepository = accountRepository
	suite.userRepository = userRepository
	suite.userTokenRepository = userTokenRepository
}

func (suite *authHandlerSuite) SetupTest() {
	suite.TruncateTables(
		"account",
		"accounts",
		"users",
		"user_tokens",
	)
}

func (suite *authHandlerSuite) TearDownSuite() {
	suite.DeleteDatabaseFile("account")
}

func (suite *authHandlerSuite) TestCreateToken() {
	r := suite.Require()

	password := gofakeit.Password(true, true, true, true, false, 8)
	account := suite.createAccount(password)

	ctx := context.TODO()
	req := &proto.CreateTokenRequest{
		Email:    account.Owner.Email,
		Password: password,
	}
	res := &proto.TokenResponse{}
	err := suite.handler.CreateToken(ctx, req, res)
	r.Nilf(err, "error creating token: %v", err)

	r.NotNil(res.Token)
	r.NotNil(res.User)
	r.Equal(res.User.Email, res.Claims["email"])
	r.Equal(string(res.User.Id), res.Claims["user_id"])
	r.Equal(string(res.User.AccountId), res.Claims["account_id"])
}

func (suite *authHandlerSuite) TestVerifyToken() {
	r := suite.Require()

	password := gofakeit.Password(true, true, true, true, false, 8)
	account := suite.createAccount(password)

	token := suite.createToken(account.Owner.Email, password)

	ctx := context.TODO()
	req := &proto.VerifyTokenRequest{
		Token: token,
	}
	res := &proto.TokenResponse{}
	err := suite.handler.VerifyToken(ctx, req, res)
	r.Nilf(err, "error verifying token: %v", err)

	r.Equal(token, res.Token)
	r.EqualValues(account.Owner.ID, res.User.Id)
}

func (suite *authHandlerSuite) TestRevokeToken() {
	r := suite.Require()

	password := gofakeit.Password(true, true, true, true, false, 8)
	account := suite.createAccount(password)

	token := suite.createToken(account.Owner.Email, password)

	ctx := context.TODO()
	revokeRequest := &proto.RevokeTokenRequest{
		Token: token,
	}
	revokeResponse := &proto.RevokeTokenResponse{}
	err := suite.handler.RevokeToken(ctx, revokeRequest, revokeResponse)
	r.Nilf(err, "error revoking token: %v", err)

	req := &proto.VerifyTokenRequest{
		Token: token,
	}
	res := &proto.TokenResponse{}
	err = suite.handler.VerifyToken(ctx, req, res)
	r.NotNilf(err, "token should be invalid")

	r.Equal(err, errors.New("invalid token"))
}

func TestAuthHandlerSuite(t *testing.T) {
	suite.Run(t, &authHandlerSuite{})
}
