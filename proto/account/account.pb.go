// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/account/account.proto

package account

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type ListUsersRequest struct {
	AccountId            int64    `protobuf:"varint,1,opt,name=account_id,json=accountId,proto3" json:"account_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListUsersRequest) Reset()         { *m = ListUsersRequest{} }
func (m *ListUsersRequest) String() string { return proto.CompactTextString(m) }
func (*ListUsersRequest) ProtoMessage()    {}
func (*ListUsersRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{0}
}
func (m *ListUsersRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListUsersRequest.Unmarshal(m, b)
}
func (m *ListUsersRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListUsersRequest.Marshal(b, m, deterministic)
}
func (dst *ListUsersRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListUsersRequest.Merge(dst, src)
}
func (m *ListUsersRequest) XXX_Size() int {
	return xxx_messageInfo_ListUsersRequest.Size(m)
}
func (m *ListUsersRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListUsersRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListUsersRequest proto.InternalMessageInfo

func (m *ListUsersRequest) GetAccountId() int64 {
	if m != nil {
		return m.AccountId
	}
	return 0
}

type ListUsersResponse struct {
	AccountId            int64           `protobuf:"varint,1,opt,name=account_id,json=accountId,proto3" json:"account_id,omitempty"`
	Users                []*UserResponse `protobuf:"bytes,2,rep,name=users,proto3" json:"users,omitempty"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *ListUsersResponse) Reset()         { *m = ListUsersResponse{} }
func (m *ListUsersResponse) String() string { return proto.CompactTextString(m) }
func (*ListUsersResponse) ProtoMessage()    {}
func (*ListUsersResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{1}
}
func (m *ListUsersResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListUsersResponse.Unmarshal(m, b)
}
func (m *ListUsersResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListUsersResponse.Marshal(b, m, deterministic)
}
func (dst *ListUsersResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListUsersResponse.Merge(dst, src)
}
func (m *ListUsersResponse) XXX_Size() int {
	return xxx_messageInfo_ListUsersResponse.Size(m)
}
func (m *ListUsersResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ListUsersResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ListUsersResponse proto.InternalMessageInfo

func (m *ListUsersResponse) GetAccountId() int64 {
	if m != nil {
		return m.AccountId
	}
	return 0
}

func (m *ListUsersResponse) GetUsers() []*UserResponse {
	if m != nil {
		return m.Users
	}
	return nil
}

type FindAccountRequest struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Slug                 string   `protobuf:"bytes,2,opt,name=slug,proto3" json:"slug,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FindAccountRequest) Reset()         { *m = FindAccountRequest{} }
func (m *FindAccountRequest) String() string { return proto.CompactTextString(m) }
func (*FindAccountRequest) ProtoMessage()    {}
func (*FindAccountRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{2}
}
func (m *FindAccountRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FindAccountRequest.Unmarshal(m, b)
}
func (m *FindAccountRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FindAccountRequest.Marshal(b, m, deterministic)
}
func (dst *FindAccountRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FindAccountRequest.Merge(dst, src)
}
func (m *FindAccountRequest) XXX_Size() int {
	return xxx_messageInfo_FindAccountRequest.Size(m)
}
func (m *FindAccountRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_FindAccountRequest.DiscardUnknown(m)
}

var xxx_messageInfo_FindAccountRequest proto.InternalMessageInfo

func (m *FindAccountRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *FindAccountRequest) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

type ListAccountsRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListAccountsRequest) Reset()         { *m = ListAccountsRequest{} }
func (m *ListAccountsRequest) String() string { return proto.CompactTextString(m) }
func (*ListAccountsRequest) ProtoMessage()    {}
func (*ListAccountsRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{3}
}
func (m *ListAccountsRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListAccountsRequest.Unmarshal(m, b)
}
func (m *ListAccountsRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListAccountsRequest.Marshal(b, m, deterministic)
}
func (dst *ListAccountsRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListAccountsRequest.Merge(dst, src)
}
func (m *ListAccountsRequest) XXX_Size() int {
	return xxx_messageInfo_ListAccountsRequest.Size(m)
}
func (m *ListAccountsRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListAccountsRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListAccountsRequest proto.InternalMessageInfo

type ListAccountsResponse struct {
	Accounts             []*AccountResponse `protobuf:"bytes,1,rep,name=accounts,proto3" json:"accounts,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *ListAccountsResponse) Reset()         { *m = ListAccountsResponse{} }
func (m *ListAccountsResponse) String() string { return proto.CompactTextString(m) }
func (*ListAccountsResponse) ProtoMessage()    {}
func (*ListAccountsResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{4}
}
func (m *ListAccountsResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListAccountsResponse.Unmarshal(m, b)
}
func (m *ListAccountsResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListAccountsResponse.Marshal(b, m, deterministic)
}
func (dst *ListAccountsResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListAccountsResponse.Merge(dst, src)
}
func (m *ListAccountsResponse) XXX_Size() int {
	return xxx_messageInfo_ListAccountsResponse.Size(m)
}
func (m *ListAccountsResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ListAccountsResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ListAccountsResponse proto.InternalMessageInfo

func (m *ListAccountsResponse) GetAccounts() []*AccountResponse {
	if m != nil {
		return m.Accounts
	}
	return nil
}

type RevokeTokenRequest struct {
	Token                string   `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RevokeTokenRequest) Reset()         { *m = RevokeTokenRequest{} }
func (m *RevokeTokenRequest) String() string { return proto.CompactTextString(m) }
func (*RevokeTokenRequest) ProtoMessage()    {}
func (*RevokeTokenRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{5}
}
func (m *RevokeTokenRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RevokeTokenRequest.Unmarshal(m, b)
}
func (m *RevokeTokenRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RevokeTokenRequest.Marshal(b, m, deterministic)
}
func (dst *RevokeTokenRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RevokeTokenRequest.Merge(dst, src)
}
func (m *RevokeTokenRequest) XXX_Size() int {
	return xxx_messageInfo_RevokeTokenRequest.Size(m)
}
func (m *RevokeTokenRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_RevokeTokenRequest.DiscardUnknown(m)
}

var xxx_messageInfo_RevokeTokenRequest proto.InternalMessageInfo

func (m *RevokeTokenRequest) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type RevokeTokenResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RevokeTokenResponse) Reset()         { *m = RevokeTokenResponse{} }
func (m *RevokeTokenResponse) String() string { return proto.CompactTextString(m) }
func (*RevokeTokenResponse) ProtoMessage()    {}
func (*RevokeTokenResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{6}
}
func (m *RevokeTokenResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RevokeTokenResponse.Unmarshal(m, b)
}
func (m *RevokeTokenResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RevokeTokenResponse.Marshal(b, m, deterministic)
}
func (dst *RevokeTokenResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RevokeTokenResponse.Merge(dst, src)
}
func (m *RevokeTokenResponse) XXX_Size() int {
	return xxx_messageInfo_RevokeTokenResponse.Size(m)
}
func (m *RevokeTokenResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_RevokeTokenResponse.DiscardUnknown(m)
}

var xxx_messageInfo_RevokeTokenResponse proto.InternalMessageInfo

type CreateTokenRequest struct {
	Email                string   `protobuf:"bytes,1,opt,name=email,proto3" json:"email,omitempty"`
	Password             string   `protobuf:"bytes,2,opt,name=password,proto3" json:"password,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateTokenRequest) Reset()         { *m = CreateTokenRequest{} }
func (m *CreateTokenRequest) String() string { return proto.CompactTextString(m) }
func (*CreateTokenRequest) ProtoMessage()    {}
func (*CreateTokenRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{7}
}
func (m *CreateTokenRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateTokenRequest.Unmarshal(m, b)
}
func (m *CreateTokenRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateTokenRequest.Marshal(b, m, deterministic)
}
func (dst *CreateTokenRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateTokenRequest.Merge(dst, src)
}
func (m *CreateTokenRequest) XXX_Size() int {
	return xxx_messageInfo_CreateTokenRequest.Size(m)
}
func (m *CreateTokenRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateTokenRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateTokenRequest proto.InternalMessageInfo

func (m *CreateTokenRequest) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *CreateTokenRequest) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

type TokenResponse struct {
	Token                string            `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	Claims               map[string]string `protobuf:"bytes,2,rep,name=claims,proto3" json:"claims,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	User                 *UserResponse     `protobuf:"bytes,3,opt,name=user,proto3" json:"user,omitempty"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *TokenResponse) Reset()         { *m = TokenResponse{} }
func (m *TokenResponse) String() string { return proto.CompactTextString(m) }
func (*TokenResponse) ProtoMessage()    {}
func (*TokenResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{8}
}
func (m *TokenResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TokenResponse.Unmarshal(m, b)
}
func (m *TokenResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TokenResponse.Marshal(b, m, deterministic)
}
func (dst *TokenResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TokenResponse.Merge(dst, src)
}
func (m *TokenResponse) XXX_Size() int {
	return xxx_messageInfo_TokenResponse.Size(m)
}
func (m *TokenResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_TokenResponse.DiscardUnknown(m)
}

var xxx_messageInfo_TokenResponse proto.InternalMessageInfo

func (m *TokenResponse) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

func (m *TokenResponse) GetClaims() map[string]string {
	if m != nil {
		return m.Claims
	}
	return nil
}

func (m *TokenResponse) GetUser() *UserResponse {
	if m != nil {
		return m.User
	}
	return nil
}

type VerifyTokenRequest struct {
	Token                string   `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *VerifyTokenRequest) Reset()         { *m = VerifyTokenRequest{} }
func (m *VerifyTokenRequest) String() string { return proto.CompactTextString(m) }
func (*VerifyTokenRequest) ProtoMessage()    {}
func (*VerifyTokenRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{9}
}
func (m *VerifyTokenRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_VerifyTokenRequest.Unmarshal(m, b)
}
func (m *VerifyTokenRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_VerifyTokenRequest.Marshal(b, m, deterministic)
}
func (dst *VerifyTokenRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_VerifyTokenRequest.Merge(dst, src)
}
func (m *VerifyTokenRequest) XXX_Size() int {
	return xxx_messageInfo_VerifyTokenRequest.Size(m)
}
func (m *VerifyTokenRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_VerifyTokenRequest.DiscardUnknown(m)
}

var xxx_messageInfo_VerifyTokenRequest proto.InternalMessageInfo

func (m *VerifyTokenRequest) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type UserRequest struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	AccountId            int64    `protobuf:"varint,2,opt,name=account_id,json=accountId,proto3" json:"account_id,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	Email                string   `protobuf:"bytes,4,opt,name=email,proto3" json:"email,omitempty"`
	Password             string   `protobuf:"bytes,5,opt,name=password,proto3" json:"password,omitempty"`
	PasswordConfirmation string   `protobuf:"bytes,6,opt,name=password_confirmation,json=passwordConfirmation,proto3" json:"password_confirmation,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserRequest) Reset()         { *m = UserRequest{} }
func (m *UserRequest) String() string { return proto.CompactTextString(m) }
func (*UserRequest) ProtoMessage()    {}
func (*UserRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{10}
}
func (m *UserRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserRequest.Unmarshal(m, b)
}
func (m *UserRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserRequest.Marshal(b, m, deterministic)
}
func (dst *UserRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserRequest.Merge(dst, src)
}
func (m *UserRequest) XXX_Size() int {
	return xxx_messageInfo_UserRequest.Size(m)
}
func (m *UserRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UserRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UserRequest proto.InternalMessageInfo

func (m *UserRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *UserRequest) GetAccountId() int64 {
	if m != nil {
		return m.AccountId
	}
	return 0
}

func (m *UserRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *UserRequest) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *UserRequest) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

func (m *UserRequest) GetPasswordConfirmation() string {
	if m != nil {
		return m.PasswordConfirmation
	}
	return ""
}

type AccountRequest struct {
	Id                   int64        `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string       `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Owner                *UserRequest `protobuf:"bytes,3,opt,name=owner,proto3" json:"owner,omitempty"`
	XXX_NoUnkeyedLiteral struct{}     `json:"-"`
	XXX_unrecognized     []byte       `json:"-"`
	XXX_sizecache        int32        `json:"-"`
}

func (m *AccountRequest) Reset()         { *m = AccountRequest{} }
func (m *AccountRequest) String() string { return proto.CompactTextString(m) }
func (*AccountRequest) ProtoMessage()    {}
func (*AccountRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{11}
}
func (m *AccountRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AccountRequest.Unmarshal(m, b)
}
func (m *AccountRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AccountRequest.Marshal(b, m, deterministic)
}
func (dst *AccountRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AccountRequest.Merge(dst, src)
}
func (m *AccountRequest) XXX_Size() int {
	return xxx_messageInfo_AccountRequest.Size(m)
}
func (m *AccountRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_AccountRequest.DiscardUnknown(m)
}

var xxx_messageInfo_AccountRequest proto.InternalMessageInfo

func (m *AccountRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *AccountRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *AccountRequest) GetOwner() *UserRequest {
	if m != nil {
		return m.Owner
	}
	return nil
}

type UserResponse struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	AccountId            int64    `protobuf:"varint,2,opt,name=account_id,json=accountId,proto3" json:"account_id,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	Email                string   `protobuf:"bytes,4,opt,name=email,proto3" json:"email,omitempty"`
	CreatedAt            string   `protobuf:"bytes,5,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,6,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserResponse) Reset()         { *m = UserResponse{} }
func (m *UserResponse) String() string { return proto.CompactTextString(m) }
func (*UserResponse) ProtoMessage()    {}
func (*UserResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{12}
}
func (m *UserResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserResponse.Unmarshal(m, b)
}
func (m *UserResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserResponse.Marshal(b, m, deterministic)
}
func (dst *UserResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserResponse.Merge(dst, src)
}
func (m *UserResponse) XXX_Size() int {
	return xxx_messageInfo_UserResponse.Size(m)
}
func (m *UserResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UserResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UserResponse proto.InternalMessageInfo

func (m *UserResponse) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *UserResponse) GetAccountId() int64 {
	if m != nil {
		return m.AccountId
	}
	return 0
}

func (m *UserResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *UserResponse) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *UserResponse) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *UserResponse) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type AccountResponse struct {
	Id                   int64         `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	OwnerId              int64         `protobuf:"varint,2,opt,name=owner_id,json=ownerId,proto3" json:"owner_id,omitempty"`
	Name                 string        `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	Slug                 string        `protobuf:"bytes,4,opt,name=slug,proto3" json:"slug,omitempty"`
	Owner                *UserResponse `protobuf:"bytes,5,opt,name=owner,proto3" json:"owner,omitempty"`
	CreatedAt            string        `protobuf:"bytes,6,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            string        `protobuf:"bytes,7,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *AccountResponse) Reset()         { *m = AccountResponse{} }
func (m *AccountResponse) String() string { return proto.CompactTextString(m) }
func (*AccountResponse) ProtoMessage()    {}
func (*AccountResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_account_bf65cce576c09064, []int{13}
}
func (m *AccountResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AccountResponse.Unmarshal(m, b)
}
func (m *AccountResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AccountResponse.Marshal(b, m, deterministic)
}
func (dst *AccountResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AccountResponse.Merge(dst, src)
}
func (m *AccountResponse) XXX_Size() int {
	return xxx_messageInfo_AccountResponse.Size(m)
}
func (m *AccountResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_AccountResponse.DiscardUnknown(m)
}

var xxx_messageInfo_AccountResponse proto.InternalMessageInfo

func (m *AccountResponse) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *AccountResponse) GetOwnerId() int64 {
	if m != nil {
		return m.OwnerId
	}
	return 0
}

func (m *AccountResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *AccountResponse) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

func (m *AccountResponse) GetOwner() *UserResponse {
	if m != nil {
		return m.Owner
	}
	return nil
}

func (m *AccountResponse) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *AccountResponse) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

func init() {
	proto.RegisterType((*ListUsersRequest)(nil), "account.ListUsersRequest")
	proto.RegisterType((*ListUsersResponse)(nil), "account.ListUsersResponse")
	proto.RegisterType((*FindAccountRequest)(nil), "account.FindAccountRequest")
	proto.RegisterType((*ListAccountsRequest)(nil), "account.ListAccountsRequest")
	proto.RegisterType((*ListAccountsResponse)(nil), "account.ListAccountsResponse")
	proto.RegisterType((*RevokeTokenRequest)(nil), "account.RevokeTokenRequest")
	proto.RegisterType((*RevokeTokenResponse)(nil), "account.RevokeTokenResponse")
	proto.RegisterType((*CreateTokenRequest)(nil), "account.CreateTokenRequest")
	proto.RegisterType((*TokenResponse)(nil), "account.TokenResponse")
	proto.RegisterMapType((map[string]string)(nil), "account.TokenResponse.ClaimsEntry")
	proto.RegisterType((*VerifyTokenRequest)(nil), "account.VerifyTokenRequest")
	proto.RegisterType((*UserRequest)(nil), "account.UserRequest")
	proto.RegisterType((*AccountRequest)(nil), "account.AccountRequest")
	proto.RegisterType((*UserResponse)(nil), "account.UserResponse")
	proto.RegisterType((*AccountResponse)(nil), "account.AccountResponse")
}

func init() {
	proto.RegisterFile("proto/account/account.proto", fileDescriptor_account_bf65cce576c09064)
}

var fileDescriptor_account_bf65cce576c09064 = []byte{
	// 681 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x55, 0xcb, 0x6e, 0xd3, 0x40,
	0x14, 0xd5, 0x38, 0x4e, 0x52, 0xdf, 0xf4, 0xc5, 0x90, 0x82, 0xeb, 0xb6, 0x52, 0x34, 0xab, 0x52,
	0xa4, 0x22, 0x5a, 0x16, 0x6d, 0x77, 0x49, 0xa1, 0x6a, 0x45, 0x57, 0x16, 0x65, 0x1b, 0x4c, 0x3c,
	0x05, 0xab, 0x89, 0x1d, 0xfc, 0x68, 0x95, 0x0f, 0xe2, 0x1b, 0xd8, 0xb2, 0x63, 0xc9, 0x96, 0x0f,
	0xe0, 0x43, 0xd0, 0xbc, 0xdc, 0x71, 0x6c, 0xa7, 0xb0, 0x60, 0x95, 0xb9, 0x33, 0xf7, 0x9e, 0x9c,
	0x7b, 0xe6, 0xfa, 0x0c, 0x6c, 0x4d, 0xe3, 0x28, 0x8d, 0x5e, 0x78, 0xa3, 0x51, 0x94, 0x85, 0xa9,
	0xfa, 0xdd, 0xe7, 0xbb, 0xb8, 0x2d, 0x43, 0xf2, 0x12, 0xd6, 0x2f, 0x83, 0x24, 0xbd, 0x4a, 0x68,
	0x9c, 0xb8, 0xf4, 0x4b, 0x46, 0x93, 0x14, 0xef, 0x00, 0xc8, 0xe3, 0x61, 0xe0, 0xdb, 0xa8, 0x87,
	0x76, 0x1b, 0xae, 0x25, 0x77, 0x2e, 0x7c, 0x32, 0x84, 0x47, 0x5a, 0x49, 0x32, 0x8d, 0xc2, 0x84,
	0x3e, 0x50, 0x83, 0x9f, 0x43, 0x33, 0x63, 0xf9, 0xb6, 0xd1, 0x6b, 0xec, 0x76, 0x0e, 0x36, 0xf6,
	0x15, 0x1d, 0x86, 0xa2, 0x40, 0x5c, 0x91, 0x43, 0x8e, 0x00, 0x9f, 0x05, 0xa1, 0xdf, 0x17, 0x29,
	0x8a, 0xd5, 0x2a, 0x18, 0x39, 0xb2, 0x11, 0xf8, 0x18, 0x83, 0x99, 0x8c, 0xb3, 0x4f, 0xb6, 0xd1,
	0x43, 0xbb, 0x96, 0xcb, 0xd7, 0x64, 0x03, 0x1e, 0x33, 0x6a, 0xb2, 0x52, 0x35, 0x44, 0x2e, 0xa1,
	0x5b, 0xdc, 0x96, 0xa4, 0x5f, 0xc1, 0x92, 0xe4, 0x91, 0xd8, 0x88, 0x13, 0xb3, 0x73, 0x62, 0xf9,
	0xbf, 0x4b, 0x6e, 0x79, 0x26, 0xd9, 0x03, 0xec, 0xd2, 0xdb, 0xe8, 0x86, 0xbe, 0x8b, 0x6e, 0x68,
	0xa8, 0xe8, 0x75, 0xa1, 0x99, 0xb2, 0x98, 0x33, 0xb4, 0x5c, 0x11, 0x30, 0x42, 0x85, 0x5c, 0x01,
	0x46, 0xce, 0x00, 0x9f, 0xc6, 0xd4, 0x4b, 0x4b, 0x10, 0x74, 0xe2, 0x05, 0x63, 0x05, 0xc1, 0x03,
	0xec, 0xc0, 0xd2, 0xd4, 0x4b, 0x92, 0xbb, 0x28, 0xf6, 0x65, 0xaf, 0x79, 0x4c, 0x7e, 0x20, 0x58,
	0x29, 0x20, 0x57, 0xd3, 0xc0, 0x27, 0xd0, 0x1a, 0x8d, 0xbd, 0x60, 0xa2, 0xf4, 0x27, 0x79, 0x9b,
	0x85, 0xea, 0xfd, 0x53, 0x9e, 0xf4, 0x26, 0x4c, 0xe3, 0x99, 0x2b, 0x2b, 0xf0, 0x33, 0x30, 0xd9,
	0xb5, 0xd8, 0x8d, 0x1e, 0xaa, 0xbf, 0x39, 0x9e, 0xe2, 0x1c, 0x43, 0x47, 0x43, 0xc0, 0xeb, 0xd0,
	0xb8, 0xa1, 0x33, 0xc9, 0x84, 0x2d, 0x19, 0xbb, 0x5b, 0x6f, 0x9c, 0x51, 0xd9, 0x88, 0x08, 0x4e,
	0x8c, 0x23, 0xc4, 0x44, 0x7d, 0x4f, 0xe3, 0xe0, 0x7a, 0xf6, 0x17, 0xa2, 0x7e, 0x43, 0xd0, 0x11,
	0xff, 0x5e, 0x3d, 0x19, 0xc5, 0x59, 0x34, 0xe6, 0x67, 0x11, 0x83, 0x19, 0x7a, 0x13, 0xca, 0x1b,
	0xb2, 0x5c, 0xbe, 0xbe, 0x97, 0xde, 0xac, 0x93, 0xbe, 0x59, 0x94, 0x1e, 0x1f, 0xc2, 0x86, 0x5a,
	0x0f, 0x47, 0x51, 0x78, 0x1d, 0xc4, 0x13, 0x2f, 0x0d, 0xa2, 0xd0, 0x6e, 0xf1, 0xc4, 0xae, 0x3a,
	0x3c, 0xd5, 0xce, 0xc8, 0x07, 0x58, 0x7d, 0x78, 0xaa, 0x39, 0x39, 0x43, 0x23, 0xb7, 0x07, 0xcd,
	0xe8, 0x2e, 0xcc, 0xaf, 0xa0, 0x3b, 0x77, 0x05, 0x1c, 0xc8, 0x15, 0x29, 0xe4, 0x2b, 0x82, 0x65,
	0xfd, 0x66, 0xfe, 0x9f, 0x38, 0x3b, 0x00, 0x23, 0x3e, 0xc3, 0xfe, 0xd0, 0x4b, 0xa5, 0x3c, 0x96,
	0xdc, 0xe9, 0x73, 0x13, 0xc9, 0xa6, 0xbe, 0x3a, 0x16, 0xa2, 0x58, 0x72, 0xa7, 0x9f, 0x92, 0x9f,
	0x08, 0xd6, 0xe6, 0x3e, 0xb1, 0x12, 0xd5, 0x4d, 0x58, 0xe2, 0x4d, 0xdd, 0x13, 0x6d, 0xf3, 0xb8,
	0x86, 0xa6, 0x32, 0x04, 0xf3, 0xde, 0x10, 0x98, 0xef, 0x08, 0xe9, 0x9a, 0x8b, 0xa6, 0x57, 0xe4,
	0xcc, 0x75, 0xd4, 0x5a, 0xdc, 0x51, 0x7b, 0xae, 0xa3, 0x83, 0xdf, 0x08, 0xda, 0xb2, 0x23, 0x3c,
	0x80, 0x15, 0xf1, 0x7d, 0xab, 0x8d, 0xa7, 0x65, 0x5f, 0xe1, 0xd7, 0xe6, 0xd4, 0x1a, 0x0e, 0x3e,
	0x87, 0x35, 0xcd, 0x05, 0x07, 0xb3, 0x8b, 0xd7, 0x78, 0x2b, 0x4f, 0x2e, 0xfb, 0xe3, 0x02, 0xa4,
	0xb7, 0xb0, 0xac, 0xdb, 0x1f, 0xde, 0xce, 0x33, 0x2b, 0xcc, 0xd2, 0xd9, 0xa9, 0x39, 0x15, 0x60,
	0x07, 0xdf, 0x11, 0x98, 0x4c, 0x3c, 0x7c, 0x0c, 0x20, 0x7a, 0xe4, 0x51, 0xe5, 0x50, 0x3a, 0xd5,
	0x7a, 0xb3, 0xd2, 0x2b, 0xae, 0xdb, 0xbf, 0x97, 0x0e, 0xc0, 0xca, 0x1f, 0x1f, 0xbc, 0x59, 0xa0,
	0xaa, 0xbf, 0x61, 0x8e, 0x53, 0x75, 0x24, 0x5b, 0xf8, 0x85, 0xc0, 0xec, 0x67, 0xe9, 0x67, 0x3c,
	0x80, 0x8e, 0x66, 0xc3, 0x9a, 0xbc, 0x65, 0x73, 0x76, 0x9e, 0x54, 0x5b, 0x26, 0xc3, 0xd0, 0x8c,
	0x4b, 0xc3, 0x28, 0xdb, 0x59, 0x2d, 0xc6, 0x39, 0x74, 0xb4, 0x57, 0x42, 0xc3, 0x28, 0xbf, 0x33,
	0xce, 0x76, 0xf5, 0xa1, 0x40, 0xfa, 0xd8, 0xe2, 0xcf, 0xfb, 0xe1, 0x9f, 0x00, 0x00, 0x00, 0xff,
	0xff, 0xcc, 0x93, 0x16, 0x36, 0xfd, 0x07, 0x00, 0x00,
}
