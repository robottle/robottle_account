package repository

import (
	"github.com/jinzhu/gorm"

	"gitlab.com/robottle/robottle_account/model"
)

// UserRepository manages users.
type UserRepository struct {
	db *gorm.DB
}

// NewUserRepository returns an instance to `UserRepository`.
func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{
		db: db,
	}
}

// CreateUser creates a new user.
func (r *UserRepository) CreateUser(user *model.User) (err error) {
	err = r.db.Create(&user).Error
	return
}

// UpdateUser updates an existing user.
func (r *UserRepository) UpdateUser(user *model.User, updates map[string]interface{}) (err error) {
	err = r.db.Model(&user).Update(updates).Error
	return
}

// FindUserByID find the user with the given `id`.
func (r *UserRepository) FindUserByID(id interface{}) (user *model.User, err error) {
	user = &model.User{}
	err = r.db.Find(&user, id).Error
	return
}

// FindUserByEmail returns the user with the given `email`.
func (r *UserRepository) FindUserByEmail(email string) (user *model.User, err error) {
	user = &model.User{}
	err = r.db.Where("email = ?", email).First(&user).Error
	return
}

// ListUsersByAccountID returns a list of users that belongs to the account with the given `accountID`.
func (r *UserRepository) ListUsersByAccountID(accountID interface{}) (users []*model.User, err error) {
	users = make([]*model.User, 0)
	err = r.db.Where("account_id = ?", accountID).Find(&users).Error
	return
}
