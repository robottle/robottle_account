package repository

import (
	"fmt"

	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
	"gitlab.com/robottle/robottle_common/random"

	"gitlab.com/robottle/robottle_account/model"
)

// AccountRepository manages accounts.
type AccountRepository struct {
	db *gorm.DB
}

// NewAccountRepository returns an instance to `AccountRepository`.
func NewAccountRepository(db *gorm.DB) *AccountRepository {
	return &AccountRepository{
		db: db,
	}
}

// CreateAccount creates a new account.
func (r *AccountRepository) CreateAccount(account *model.Account) (err error) {
	tx := r.db.Begin()
	if err = tx.Create(account).Error; err != nil {
		return
	}
	if err = tx.Model(&account.Owner).UpdateColumn("account_id", account.ID).Error; err != nil {
		return
	}
	if err = tx.Commit().Error; err != nil {
		if err = tx.Rollback().Error; err != nil {
			return
		}
		return
	}
	return
}

// ListAccounts returns a list of all active accounts.
func (r *AccountRepository) ListAccounts() (accounts []*model.Account, err error) {
	accounts = make([]*model.Account, 0)
	err = r.db.Preload("Owner").Find(&accounts).Error
	return
}

func (r *AccountRepository) FindAccountByID(id interface{}) (account *model.Account, err error) {
	account = &model.Account{}
	err = r.db.Preload("Owner").First(&account, id).Error
	return
}

func (r *AccountRepository) GenerateSlug(name string) (string, error) {
	result := make(chan string)
	errc := make(chan error)
	base := slug.Make(name)
	go func() {
		tentative := base
		for {
			exists := &model.Account{}
			if err := r.db.Where("slug = ?", tentative).First(&exists).Error; err != nil {
				if gorm.IsRecordNotFoundError(err) {
					result <- tentative
				} else {
					errc <- err
				}
				break
			}
			tentative = fmt.Sprintf("%s-%s", base, random.String(6))
		}
	}()
	select {
	case accountSlug := <-result:
		return accountSlug, nil
	case err := <-errc:
		return "", err
	}
}
