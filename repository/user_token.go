package repository

import (
	"github.com/jinzhu/gorm"

	"gitlab.com/robottle/robottle_account/model"
)

// UserTokenRepository manages user tokens.
type UserTokenRepository struct {
	db *gorm.DB
}

// NewUserTokenRepository returns an instance to `UserTokenRepository`.
func NewUserTokenRepository(db *gorm.DB) *UserTokenRepository {
	return &UserTokenRepository{
		db: db,
	}
}

// CreateUserToken creates a new user token.
func (r *UserTokenRepository) CreateUserToken(userToken *model.UserToken) (err error) {
	err = r.db.Create(&userToken).Error
	return
}

// FindUserTokenByToken returns the user token with the given `token`.
func (r *UserTokenRepository) FindUserTokenByToken(token string) (userToken *model.UserToken, err error) {
	userToken = &model.UserToken{}
	err = r.db.Where("token = ?", token).First(&userToken).Error
	return
}

// DeleteUserToken deletes an existing user token.
func (r *UserTokenRepository) DeleteUserToken(userToken *model.UserToken) (err error) {
	err = r.db.Delete(&userToken).Error
	return
}
