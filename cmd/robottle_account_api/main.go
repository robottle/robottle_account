package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/micro/cli"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-web"

	commonConfig "gitlab.com/robottle/robottle_common/config"
	commonWrapper "gitlab.com/robottle/robottle_common/wrapper"

	"gitlab.com/robottle/robottle_account/option"
	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/router"
)

const (
	ServiceName = "io.coderoso.robottle.api.account"
	Version     = "0.0.1"
)

func main() {
	var (
		configurationFile string

		errc = make(chan error)
	)
	service := web.NewService(
		web.Name(ServiceName),
		web.Version(Version),
		web.Flags(
			cli.StringFlag{
				Name:        "config, c",
				Value:       "config.yml",
				Usage:       "Path to configuration file to use. Defaults to config.yml",
				EnvVar:      "CONFIG_FILE",
				Destination: &configurationFile,
			},
		),
		web.RegisterTTL(30*time.Second),
		web.RegisterInterval(10*time.Second),
	)
	_ = service.Init()

	if err := commonConfig.SetConfigurationFile(configurationFile); err != nil {
		log.Fatalf("error setting configuration file: %v", err)
	}

	config, err := commonConfig.GetConfig()
	if err != nil {
		log.Fatalf("error getting configuration: %v", err)
	}

	serviceSettings, found := config.Service["account"]
	if !found {
		log.Fatal("no service configuration for account found")
	}

	httpSettings := config.HTTP
	if httpSettings == nil {
		log.Fatal("HTTP settings not found")
	}

	_ = client.DefaultClient.Init(
		client.WrapCall(commonWrapper.NewAuthCallWrapper()),
	)

	accountService := accountProto.NewAccountService(serviceSettings.URL(), client.DefaultClient)
	userService := accountProto.NewUserService(serviceSettings.URL(), client.DefaultClient)
	authService := accountProto.NewAuthService(serviceSettings.URL(), client.DefaultClient)

	options := option.NewOptions(
		option.WithAccountService(accountService),
		option.WithUserService(userService),
		option.WithAuthService(authService),
		option.WithAllowedHosts(httpSettings.AllowedHosts),
		option.WithAllowedMethods(httpSettings.AllowedMethods),
		option.WithAllowedHeaders(httpSettings.AllowedHeaders),
	)

	service.Handle("/", router.NewRouter(options))

	go func() {
		if err := service.Run(); err != nil {
			errc <- err
		}
	}()

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)
		errc <- fmt.Errorf("%v", <-c)
	}()

	if err := <-errc; err != nil {
		log.Fatal(err)
	}
}
