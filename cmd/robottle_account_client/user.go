package main

import (
	"github.com/micro/cli"
	"github.com/micro/go-micro/client"

	commonConfig "gitlab.com/robottle/robottle_common/config"

	proto "gitlab.com/robottle/robottle_account/proto/account"
)

func userCommand(serviceSettings *commonConfig.Service) cli.Command {
	userService := proto.NewUserService(serviceSettings.URL(), client.DefaultClient)
	return cli.Command{
		Name:  "user",
		Usage: "User operations",
		Flags: []cli.Flag{
			tokenFlag,
		},
		Subcommands: []cli.Command{
			{
				Name:  "create",
				Usage: "Creates a new user",
				Flags: []cli.Flag{
					userNameFlag,
					userEmailFlag,
					userPasswordFlag,
					userPasswordConfirmationFlag,
				},
				Category: "user",
				Action:   createUser(userService),
			},
		},
	}
}

func createUser(userService proto.UserService) func(*cli.Context) {
	return func(c *cli.Context) {

	}
}
