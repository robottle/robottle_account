package main

import (
	"fmt"
	"log"
	"os"

	"github.com/micro/cli"

	"github.com/micro/go-micro/client"

	commonConfig "gitlab.com/robottle/robottle_common/config"
	commonWrapper "gitlab.com/robottle/robottle_common/wrapper"
)

const (
	Name    = "Account service client"
	Version = "0.0.1"
	Email   = "coderoso.io@gmail.com"
	Author  = "Saul Martínez"
)

func main() {
	var (
		configurationFile string
	)

	app := cli.NewApp()
	app.Name = Name
	app.Email = Email
	app.Version = Version
	app.Author = Author
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "config,c",
			Value:       "config.yml",
			Usage:       "Path to configuration file to use. Defaults to config.yml",
			Destination: &configurationFile,
		},
	}
	app.Before = func(c *cli.Context) (err error) {
		err = commonConfig.SetConfigurationFile(configurationFile)
		if err != nil {
			return fmt.Errorf("error setting configuration file: %v", err)
		}
		return
	}

	config, err := commonConfig.GetConfig()
	if err != nil {
		log.Fatalf("error getting coniguration: %v", err)
	}

	serviceSettings, found := config.Service["account"]
	if !found {
		log.Fatalf("service configuration for account service not found")
	}

	_ = client.DefaultClient.Init(
		client.WrapCall(commonWrapper.NewAuthCallWrapper()),
	)

	app.Commands = []cli.Command{
		authCommand(serviceSettings),
		accountCommand(serviceSettings),
		userCommand(serviceSettings),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
