package main

import (
	"context"
	"log"

	"github.com/micro/cli"
	"github.com/micro/go-micro/client"

	commonConfig "gitlab.com/robottle/robottle_common/config"

	proto "gitlab.com/robottle/robottle_account/proto/account"
)

func accountCommand(serviceSettings *commonConfig.Service) cli.Command {
	accountService := proto.NewAccountService(serviceSettings.URL(), client.DefaultClient)
	return cli.Command{
		Name:  "account",
		Usage: "Account operations",
		Subcommands: []cli.Command{
			{
				Name:  "create",
				Usage: "Create a new account",
				Flags: []cli.Flag{
					accountNameFlag,
					ownerNameFlag,
					ownerEmailFlag,
					ownerPasswordFlag,
					ownerPasswordConfirmationFlag,
				},
				Category: "account",
				Action:   createAccount(accountService),
			},
		},
	}
}

func createAccount(accountService proto.AccountService) func(*cli.Context) {
	return func(c *cli.Context) {
		ctx := context.WithValue(context.Background(), "authorization", token)

		req := &proto.AccountRequest{
			Name: accountName,
			Owner: &proto.UserRequest{
				Name:                 ownerName,
				Email:                ownerEmail,
				Password:             ownerPassword,
				PasswordConfirmation: ownerPasswordConfirmation,
			},
		}
		res, err := accountService.CreateAccount(ctx, req)
		if err != nil {
			log.Fatalf("error creating account: %v", err)
		}
		log.Printf("%+v", res)
	}
}
