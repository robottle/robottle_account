package main

import "github.com/micro/cli"

var (
	accountName               string
	ownerName                 string
	ownerEmail                string
	ownerPassword             string
	ownerPasswordConfirmation string
	userName                  string
	userEmail                 string
	userPassword              string
	userPasswordConfirmation  string
	authEmail                 string
	authPassword              string
	token                     string

	accountNameFlag = cli.StringFlag{
		Name:        "account-name",
		Usage:       "Account name",
		Destination: &accountName,
	}
	ownerNameFlag = cli.StringFlag{
		Name:        "owner-name",
		Usage:       "Owner name",
		Destination: &ownerName,
	}
	ownerEmailFlag = cli.StringFlag{
		Name:        "owner-email",
		Usage:       "Owner email",
		Destination: &ownerEmail,
	}
	ownerPasswordFlag = cli.StringFlag{
		Name:        "owner-password",
		Usage:       "Owner password",
		Destination: &ownerPassword,
	}
	ownerPasswordConfirmationFlag = cli.StringFlag{
		Name:        "owner-password-confirmation",
		Usage:       "Owner password confirmation",
		Destination: &ownerPasswordConfirmation,
	}
	userNameFlag = cli.StringFlag{
		Name:        "user-name",
		Usage:       "User name",
		Destination: &userName,
	}
	userEmailFlag = cli.StringFlag{
		Name:        "user-email",
		Usage:       "User email",
		Destination: &userEmail,
	}
	userPasswordFlag = cli.StringFlag{
		Name:        "user-password",
		Usage:       "User password",
		Destination: &userPassword,
	}
	userPasswordConfirmationFlag = cli.StringFlag{
		Name:        "user-password-confirmation",
		Usage:       "User password confirmation",
		Destination: &userPasswordConfirmation,
	}
	authEmailFlag = cli.StringFlag{
		Name:        "auth-email",
		Usage:       "Authentication email",
		Destination: &authEmail,
	}
	authPasswordFlag = cli.StringFlag{
		Name:        "auth-password",
		Usage:       "Authentication password",
		Destination: &authPassword,
	}
	tokenFlag = cli.StringFlag{
		Name:        "token",
		Usage:       "Authentication token",
		Destination: &token,
	}
)
