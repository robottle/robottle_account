package main

import (
	"github.com/micro/cli"
	"github.com/micro/go-micro/client"

	commonConfig "gitlab.com/robottle/robottle_common/config"

	proto "gitlab.com/robottle/robottle_account/proto/account"
)

func authCommand(serviceSettings *commonConfig.Service) cli.Command {
	authService := proto.NewAuthService(serviceSettings.URL(), client.DefaultClient)
	return cli.Command{
		Name:  "auth",
		Usage: "Authentication operations",
		Subcommands: []cli.Command{
			{
				Name:  "create-token",
				Usage: "Create a new authentication token",
				Flags: []cli.Flag{
					authEmailFlag,
					authPasswordFlag,
				},
				Category: "auth",
				Action:   createToken(authService),
			},
		},
	}
}

func createToken(authService proto.AuthService) func(*cli.Context) {
	return func(c *cli.Context) {

	}
}
