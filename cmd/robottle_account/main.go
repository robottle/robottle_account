package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/micro/go-micro/client"

	"gitlab.com/robottle/robottle_account/model"

	"github.com/micro/go-micro/server"

	"github.com/micro/cli"
	"github.com/micro/go-micro"

	"gitlab.com/robottle/robottle_common/auth"
	commonConfig "gitlab.com/robottle/robottle_common/config"
	"gitlab.com/robottle/robottle_common/connection"
	"gitlab.com/robottle/robottle_common/hashing"
	commonWrapper "gitlab.com/robottle/robottle_common/wrapper"

	"gitlab.com/robottle/robottle_account/handler"
	"gitlab.com/robottle/robottle_account/option"
	proto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/repository"
)

const (
	// ServiceName defines the service name.
	ServiceName string = "io.coderoso.robottle.svc.account"
	// ServiceVersion defines the service version.
	ServiceVersion string = "0.0.1"
)

func main() {
	var (
		configurationFile string

		errc = make(chan error)
	)
	service := micro.NewService(
		micro.Name(ServiceName),
		micro.Version(ServiceVersion),
		micro.Flags(
			cli.StringFlag{
				Name:        "config, c",
				Value:       "config.yml",
				Usage:       "Path to configuration file to use. Defaults to config.yml",
				EnvVar:      "CONFIG_FILE",
				Destination: &configurationFile,
			},
		),
		micro.RegisterTTL(30*time.Second),
		micro.RegisterInterval(10*time.Second),
	)
	service.Init()

	if err := commonConfig.SetConfigurationFile(configurationFile); err != nil {
		log.Fatalf("error settings configuration file %v", err)
	}

	config, err := commonConfig.GetConfig()
	if err != nil {
		log.Fatalf("error getting configuration: %v", err)
	}

	db, err := connection.GetDatabaseConnection("account")
	if err != nil {
		log.Fatalf("error getting database for account service: %v", err)
	}
	if err := model.Migrate(db); err != nil {
		log.Fatalf("error migrating models: %v", err)
	}

	hashingStrategy, err := hashing.NewHashingStrategy(config.Hashing)
	if err != nil {
		log.Fatalf("error getting hashing strategy: %v", err)
	}

	authStrategy, err := auth.NewAuthStrategy(config.Auth)
	if err != nil {
		log.Fatalf("error getting authentication strategy: %v", err)
	}

	accountCreatedPublisher := micro.NewPublisher("account_created", client.DefaultClient)

	accountRepository := repository.NewAccountRepository(db)
	userRepository := repository.NewUserRepository(db)
	userTokenRepository := repository.NewUserTokenRepository(db)

	options := option.NewOptions(
		option.WithHashingStrategy(hashingStrategy),
		option.WithAuthStrategy(authStrategy),
		option.WithAccountRepository(accountRepository),
		option.WithUserRepository(userRepository),
		option.WithUserTokenRepository(userTokenRepository),
		option.WithAccountCreatedPublisher(accountCreatedPublisher),
	)

	accountHandler := handler.NewAccountHandler(options)
	if err := proto.RegisterAccountHandler(service.Server(), accountHandler); err != nil {
		log.Fatalf("error registering account handler: %v", err)
	}

	userHandler := handler.NewUserHandler(options)
	if err := proto.RegisterUserHandler(service.Server(), userHandler); err != nil {
		log.Fatalf("error registering user handler: %v", err)
	}

	authHandler := handler.NewAuthHandler(options)
	if err := proto.RegisterAuthHandler(service.Server(), authHandler); err != nil {
		log.Fatalf("error registering auth handler: %v", err)
	}

	_ = service.Server().Init(
		server.WrapHandler(
			commonWrapper.NewAuthHandlerWrapper(
				authStrategy,
				config.Auth.AllowedMethods,
			),
		),
	)

	go func() {
		if err := service.Run(); err != nil {
			errc <- err
		}
	}()

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)
		errc <- fmt.Errorf("%v", <-c)
	}()

	if err := <-errc; err != nil {
		log.Fatal(err)
	}
}
