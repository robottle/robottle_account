module gitlab.com/robottle/robottle_account

go 1.12

require (
	github.com/brianvoe/gofakeit v3.17.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v0.0.0-20190424000812-bd1331c62cae
	github.com/gin-gonic/gin v1.3.0
	github.com/go-ozzo/ozzo-validation v3.5.0+incompatible
	github.com/gobuffalo/buffalo v0.14.3 // indirect
	github.com/gobuffalo/helpers v0.0.0-20190425165706-cddb8b03d1d1 // indirect
	github.com/gobuffalo/mw-csrf v0.0.0-20190129204204-25460a055517 // indirect
	github.com/gobuffalo/packd v0.1.0 // indirect
	github.com/gobuffalo/suite v2.6.2+incompatible
	github.com/golang/protobuf v1.3.1
	github.com/gosimple/slug v1.5.0
	github.com/jinzhu/gorm v1.9.5
	github.com/micro/cli v0.1.0
	github.com/micro/go-micro v1.1.0
	github.com/micro/go-web v1.0.0
	github.com/neelance/parallel v0.0.0-20160708114440-4de9ce63d14c // indirect
	github.com/opentracing/basictracer-go v1.0.0 // indirect
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/slimsag/godocmd v0.0.0-20161025000126-a1005ad29fe3 // indirect
	github.com/sourcegraph/ctxvfs v0.0.0-20180418081416-2b65f1b1ea81 // indirect
	github.com/sourcegraph/go-langserver v2.0.0+incompatible // indirect
	github.com/sourcegraph/jsonrpc2 v0.0.0-20190106185902-35a74f039c6a // indirect
	github.com/stretchr/testify v1.3.0
	gitlab.com/robottle/robottle_common v0.0.0-20190502075711-c703ea102562
	golang.org/x/build v0.0.0-20190111050920-041ab4dc3f9d // indirect
	grpc.go4.org v0.0.0-20170609214715-11d0a25b4919
)

replace gitlab.com/robottle/robottle_common => ../robottle_common
