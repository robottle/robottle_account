package validate

import (
	"github.com/go-ozzo/ozzo-validation"
)

type AccountRequestValidation struct {
	Name  string                 `json:"name"`
	Owner *UserRequestValidation `json:"owner"`
}

func (v *AccountRequestValidation) Validate() (err error) {
	err = validation.ValidateStruct(
		v,
		validation.Field(&v.Name, validation.Required),
		validation.Field(&v.Owner),
	)
	return
}
