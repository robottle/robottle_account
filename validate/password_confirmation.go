package validate

import (
	"fmt"

	"github.com/go-ozzo/ozzo-validation"
)

func validatePasswordConfirmation(req *UserRequestValidation) validation.RuleFunc {
	return func(value interface{}) (err error) {
		confirmation, _ := value.(string)
		if confirmation != req.Password {
			err = fmt.Errorf("does not match password")
		}
		return
	}
}
