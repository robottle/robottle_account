package validate

import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type AuthRequestValidation struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (v *AuthRequestValidation) Validate() (err error) {
	err = validation.ValidateStruct(
		v,
		validation.Field(&v.Email, validation.Required, is.Email),
		validation.Field(&v.Password, validation.Required),
	)
	return
}
