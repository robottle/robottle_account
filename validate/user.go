package validate

import (
	"strings"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type UserRequestValidation struct {
	ID                   int64  `json:"id"`
	AccountID            int64  `json:"account_id"`
	Name                 string `json:"name"`
	Email                string `json:"email"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"password_confirmation"`
}

func (v *UserRequestValidation) ValidateCreate() (err error) {
	err = validation.ValidateStruct(
		v,
		validation.Field(&v.AccountID, validation.Required),
		validation.Field(&v.Name, validation.Required),
		validation.Field(&v.Email, validation.Required, is.Email),
		validation.Field(&v.Password, validation.Required, validation.Length(6, 100)),
		validation.Field(&v.PasswordConfirmation, validation.Required, validation.By(validatePasswordConfirmation(v))),
	)
	return
}

func (v *UserRequestValidation) ValidateUpdate() (err error) {
	rules := make([]*validation.FieldRules, 0)
	if len(strings.TrimSpace(v.Password)) > 0 {
		rule := validation.Field(&v.PasswordConfirmation, validation.Required, validation.By(validatePasswordConfirmation(v)))
		rules = append(rules, rule)
	}
	err = validation.ValidateStruct(v, rules...)
	return
}
