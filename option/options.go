package option

import (
	"github.com/micro/go-micro"
	"gitlab.com/robottle/robottle_common/auth"
	"gitlab.com/robottle/robottle_common/hashing"

	proto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_account/repository"
)

// Options is a list of options.
type Options struct {
	AccountRepository       *repository.AccountRepository
	UserRepository          *repository.UserRepository
	UserTokenRepository     *repository.UserTokenRepository
	HashingStrategy         hashing.Strategy
	AuthStrategy            auth.Strategy
	AuthService             proto.AuthService
	AccountService          proto.AccountService
	UserService             proto.UserService
	AllowedHosts            []string
	AllowedMethods          []string
	AllowedHeaders          []string
	AccountCreatedPublisher micro.Publisher
}

// Option is a function to set options.
type Option func(*Options)

func WithAccountCreatedPublisher(publisher micro.Publisher) Option {
	return func(options *Options) {
		options.AccountCreatedPublisher = publisher
	}
}

// WithAllowedHosts sets the allowed hosts.
func WithAllowedHosts(allowedHosts []string) Option {
	return func(options *Options) {
		options.AllowedHosts = allowedHosts
	}
}

// WithAllowedMethods sets the allowed methods.
func WithAllowedMethods(allowedMethods []string) Option {
	return func(options *Options) {
		options.AllowedMethods = allowedMethods
	}
}

// WithAllowedHeaders sets the allowed headers.
func WithAllowedHeaders(allowedHeaders []string) Option {
	return func(options *Options) {
		options.AllowedHeaders = allowedHeaders
	}
}

// WithAccountService sets the account service to use.
func WithAccountService(accountService proto.AccountService) Option {
	return func(options *Options) {
		options.AccountService = accountService
	}
}

// WithUserService sets the user service to use.
func WithUserService(userService proto.UserService) Option {
	return func(options *Options) {
		options.UserService = userService
	}
}

// WithAuthService sets the authentication service to use.
func WithAuthService(authService proto.AuthService) Option {
	return func(options *Options) {
		options.AuthService = authService
	}
}

// WithAuthStrategy sets the authentication strategy to use.
func WithAuthStrategy(authStrategy auth.Strategy) Option {
	return func(options *Options) {
		options.AuthStrategy = authStrategy
	}
}

// WithHashingStrategy sets the hashing strategy to use.
func WithHashingStrategy(hashingStrategy hashing.Strategy) Option {
	return func(options *Options) {
		options.HashingStrategy = hashingStrategy
	}
}

// WithAccountRepository sets the `repository.AccountRepository` to use.
func WithAccountRepository(accountRepository *repository.AccountRepository) Option {
	return func(options *Options) {
		options.AccountRepository = accountRepository
	}
}

// WithUserRepository sets the user repository to use.
func WithUserRepository(userRepository *repository.UserRepository) Option {
	return func(options *Options) {
		options.UserRepository = userRepository
	}
}

// WithUserTokenRepository sets the user token repository to use.
func WithUserTokenRepository(userTokenRepository *repository.UserTokenRepository) Option {
	return func(options *Options) {
		options.UserTokenRepository = userTokenRepository
	}
}

// NewOptions returns a new `*Options` object.
func NewOptions(opts ...Option) *Options {
	options := &Options{
		AllowedMethods: []string{},
		AllowedHeaders: []string{},
		AllowedHosts:   []string{},
	}

	for _, opt := range opts {
		opt(options)
	}

	return options
}
