package middleware

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	proto "gitlab.com/robottle/robottle_account/proto/account"
)

// RequireToken checks that there is a token in the request.
func RequireToken(authService proto.AuthService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authorization := ctx.GetHeader("Authorization")
		parts := strings.Split(authorization, " ")
		if len(parts) < 2 {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"status": http.StatusUnauthorized,
				"error":  "invalid authorization token",
			})
			return
		}
		authType := strings.TrimSpace(parts[0])
		switch authType {
		case "Bearer":
			fallthrough
		case "JWT":
			token := strings.TrimSpace(parts[1])
			if len(token) == 0 {
				ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"status": http.StatusUnauthorized,
					"error": "invalid authorization token",
				})
				return
			}
			ctx.Set("token", token)
			ctx.Keys["token"] = token
			req := &proto.VerifyTokenRequest{
				Token: token,
			}
			res, err := authService.VerifyToken(ctx, req)
			if err != nil {
				ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"status": http.StatusUnauthorized,
					"error": err,
				})
				return
			}
			ctx.Set("user", res.User)
		default:
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"status": http.StatusUnauthorized,
				"error": "invalid authorization type",
			})
			return
		}
		ctx.Next()
	}
}
