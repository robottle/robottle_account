package helper

import (
	"github.com/gin-gonic/gin"

	proto "gitlab.com/robottle/robottle_account/proto/account"
)

func GetToken(ctx *gin.Context) string {
	token, found := ctx.Get("token")
	if found {
		return token.(string)
	}
	return ""
}

func GetUser(ctx *gin.Context) *proto.UserResponse {
	user, found := ctx.Get("user")
	if found {
		return user.(*proto.UserResponse)
	}
	return nil
}
